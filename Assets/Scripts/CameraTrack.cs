﻿using UnityEngine;
using System.Collections;

public class CameraTrack : MonoBehaviour {

	public GameObject player;

	public float yDisplacement = 25;

	public float moveSpeed = 5.0f;

	private Vector3 newPosition;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(player!=null)
			CameraTracking ();
	}

	void CameraTracking(){
		newPosition = player.transform.position;

		newPosition.y = yDisplacement;

		transform.position = Vector3.Slerp (transform.position,
		                                   newPosition,
		                                   moveSpeed * Time.deltaTime);
	}
}
