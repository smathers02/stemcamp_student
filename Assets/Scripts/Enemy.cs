﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    GameManager gameManager;
    AudioManager audioManager;

    Animator animator;
    Collider myCollider;

    Rigidbody rb;

    public GameObject target;

    //Movement Variables
    public float moveSpeed = 2.0f;
    private float agroRange = 25.0f;
    private Vector3 rotAdj;
    private float rotSpeed = 1.0f;

    //Damage variables
    public float health = 50.0f;
    public float damage = 10.0f;
    private float attackRange = 7.0f;
    private float attackTimer;
    private float attackRate = 1.0f;

    //Stun variables
    private float stunTimer;
    private float stunTime = 0.5f;

    //Sound Variables
    public AudioClip attackSound;
    public AudioClip deathSound;

    // Use this for initialization
    void Start () {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();

        target = GameObject.FindGameObjectWithTag("Player");

        animator = GetComponent<Animator>();
        myCollider = GetComponent<Collider>();

        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update() {

        // If Alive...
        //Idle - If alive and Player not in agro range and not stunned
        if (health > 0 && target && Time.time > stunTimer) {
            //Idle - Player out of agro range
            if(Vector3.Distance(transform.position, target.transform.position) > agroRange) {
                animator.Play("Wait");
            }
            //Attack - Player in agro range
            else if (target && Vector3.Distance(transform.position, target.transform.position) <= agroRange) {
                Attack();
            }
        } 

        //Death 
        if (health <= 0) {
            animator.Play("Dead");
            myCollider.enabled = false;
        } else if (!target) {
            animator.Play("Wait");
        }
    }

    private void Attack() {

        //If in Attack Range of Player
        if (Vector3.Distance(transform.position, target.transform.position) <= attackRange && Time.time > attackTimer) {

            animator.Play("Attack");
            audioManager.PlayAudio(attackSound);
            attackTimer = Time.time + attackRate;

        }
        //Otherwise get into attack range
        else if (Vector3.Distance(transform.position, target.transform.position) > attackRange) {

            animator.Play("Walk");

            //Rotate Towards Player - Lerp
            Vector3 thisRot = (target.transform.position - transform.position).normalized;
            Quaternion qTo = Quaternion.LookRotation(thisRot);
            transform.rotation = Quaternion.Lerp(transform.rotation, qTo, rotSpeed * Time.deltaTime);

            //Move Towards Player
            transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed);
        } 
    }

    public void DealDamage() {
        target.GetComponent<Player>().takeDamage(damage);
    }

    public void takeDamage(float damage) {


			

        health -= damage;

        animator.Play("Damage");
        stunTimer = Time.time + stunTime;
        agroRange = 500.0f;

        if (health <= 0) {
            audioManager.PlayAudio(deathSound);
            this.transform.tag = "Untagged";
            rb.useGravity = false;
			gameManager.score += 50;

        }
			
    }
}
