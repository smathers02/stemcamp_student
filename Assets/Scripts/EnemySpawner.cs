﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	GameManager gameManager;

	public float spawnTime = 5.0f;
	private float spawnTimer;
	public GameObject enemy;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (gameManager.playerAlive == true)
			SpawnEnemy ();
	}

	void SpawnEnemy(){
		if (Time.time > spawnTimer) {
			Instantiate (enemy, transform.position, transform.rotation);
			spawnTimer = Time.time + spawnTime;
		}
	}
}
