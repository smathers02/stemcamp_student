﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public bool playerAlive = true;
    public bool gameOver = false;

    //Environment Variables
    public Vector3 windVector;
    public float sunLight;
	public float resourcesAvail;
	private int houseScore;

	//Score Variables
	public int score = 0;
    public float timeSurvived;
	public float timeRemaining;

    //Resource
    public int resources;
	public int solarFuel;
	public int solarCollectors = 2;
	public int houses = 1;
	public int mines = 1;
	private int frameCounter;

	// Use this for initialization
	void Start () {

        //Set Up Remaining time variables
        timeSurvived = 0;
		frameCounter = 0;
	}
	
	// Update is called once per frame
	void Update () {

        //Increment currentTime if player is alive
        if (playerAlive && timeRemaining > 0) {
            timeRemaining -= Time.deltaTime;
            timeSurvived += Time.deltaTime;
        }


            //Game Over Check - When player dies or runs out of time
        if (!playerAlive || timeRemaining <= 0) {
            gameOver = true;
		}

		if (frameCounter > 60 && !gameOver) {
			//Update resources
			solarFuel += (int) (solarCollectors * this.sunLight);
			if (solarFuel > 2500) {
				solarFuel = 2500;
			}
			resources += (int) (mines * this.resourcesAvail);

			frameCounter = 0;
		} else {
			frameCounter++;
		}



	}
}
