﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour {

    public float lavaDamage = 5;
    public float damageTime = 0.1f;
    private float damageTimer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay(Collider other) {


        //Damage Player if enter lava
        if (other.transform.tag == "Player") {

            if(Time.time > damageTimer) {
                other.transform.GetComponent<Player>().takeDamage(lavaDamage);
                damageTimer = Time.time + damageTime;
            }
        }
    }
}
