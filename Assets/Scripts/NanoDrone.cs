﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NanoDrone : MonoBehaviour {

    GameManager gameManager;
    AudioManager audioManager;

    //NanoDrone Variables
    private float scrollSpeed = 10.0f;
    private Vector3 nanoAdj;
    private bool overResource = false;
    public GameObject buildingSpawnLocation;
    public GameObject solarPowerGenerator;
	public GameObject baseMine;
	public GameObject baseHouse;

	public int solarCost = 50;
	public int houseCost = 1000;
	public int mineCost = 50;

	public int mineCount = 0;


    // Use this for initialization
    void Start () {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
    }
	
	// Update is called once per frame
	void Update () {

        Movement();

        PlaceBuilding();
	}

   

	private bool CanFly(){
		if (gameManager.solarFuel > 0) {
			return true;
		} else {
			return false;
		}
	}



	//Calculate the fuel useage when the drone is moved based of the set speed of the drone
	private void CalculateFuel(){

		int toRemove = (int)(scrollSpeed / 19000.0);
		if (toRemove <= 0) {
			toRemove = 1;
		}

		gameManager.solarFuel -= toRemove;

	}


    void PlaceBuilding() {




    }


	private bool CanBuildSolar(){
		if (gameManager.resources >= this.solarCost) {
			return true;
		} else {
			return false;
		}
	}


	private bool CanBuildMine(){
		if (gameManager.resources >= this.mineCost) {
			return true;
		} else {
			return false;
		}
	}


	private bool CanBuildHouse(){
		if (gameManager.resources >= this.houseCost && gameManager.solarFuel >= this.houseCost && mineCount >= 3) {
			return true;
		} else {
			return false;
		}
	}


	private void BuildSolar(RaycastHit hit){
		if (hit.transform.tag == "Environment") {
			Instantiate(solarPowerGenerator, buildingSpawnLocation.transform.position, buildingSpawnLocation.transform.rotation);
			gameManager.resources -= solarCost;
			gameManager.solarCollectors += 1;
			mineCount += 1;
		}
	}


	private void BuildMine(RaycastHit hit){
		if (hit.transform.tag == "Environment") {
			Vector3 spawnSpot = buildingSpawnLocation.transform.position + new Vector3(0.0f, 1.5f, 0.0f);
			Instantiate(baseMine, spawnSpot, buildingSpawnLocation.transform.rotation);
			gameManager.resources -= mineCost;
			gameManager.mines += 1;
			mineCount += 1;

		}
	}

	private void BuildHouse(RaycastHit hit){
		if (hit.transform.tag == "Environment") {
			Vector3 spawnSpot = buildingSpawnLocation.transform.position + new Vector3(0.0f, 1.5f, 0.0f);
			Instantiate(baseHouse, spawnSpot, buildingSpawnLocation.transform.rotation);
			gameManager.resources -= houseCost;
			gameManager.solarFuel -= houseCost;
			gameManager.score += 100;
			gameManager.houses += 1;
			mineCount -= 3;

		}
	}



	void Movement() {

		//TRANSLATION - Move in direction of controller axis variables
		nanoAdj = transform.position;

		if (CanFly ()) {
			// Left Analogue Stick - X Axis Left & Right
			if (Input.GetAxis("ContHorizontal") < 0 ||  Input.GetKey (KeyCode.LeftArrow)) {
				nanoAdj.x = nanoAdj.x - scrollSpeed * Time.deltaTime;
				CalculateFuel ();
			}
			else if (Input.GetAxis("ContHorizontal") > 0 || Input.GetKey (KeyCode.RightArrow)) {
				nanoAdj.x = nanoAdj.x + scrollSpeed * Time.deltaTime;
				CalculateFuel ();
			}

			// Left Analogue Stick - Z Axis Up & Down
			if (Input.GetAxis("ContVertical") < 0 || Input.GetKey (KeyCode.DownArrow)) {
				nanoAdj.z = nanoAdj.z - scrollSpeed * Time.deltaTime;
				CalculateFuel ();
			}
			else if (Input.GetAxis("ContVertical") > 0 || Input.GetKey (KeyCode.UpArrow)) {
				nanoAdj.z = nanoAdj.z + scrollSpeed * Time.deltaTime;
				CalculateFuel ();
			}
		}


		//ROTATION - Rotate slowly towards new position vector
		Vector3 nanoRot = (nanoAdj - transform.position).normalized;
		Quaternion qTo = Quaternion.LookRotation(nanoRot);
		transform.rotation = Quaternion.Slerp(transform.rotation, qTo, scrollSpeed * Time.deltaTime);

		//WIND - Push drone with wind
		nanoAdj = nanoAdj + gameManager.windVector * Time.deltaTime;

		//Final positioning set
		transform.position = nanoAdj;
	}


    //Students to do
    private void OnTriggerStay(Collider other) {

		if(Input.GetKeyDown("joystick 1 button 0") || Input.GetKey("p")) {
			if (other.tag == "Resource") {
				Destroy(other.gameObject);
				gameManager.resources += 25;
			}

			if (other.tag == "Resource2") {
				Destroy(other.gameObject);
				gameManager.resources += 50;
			}

			if (other.tag == "Resource3") {
				Destroy(other.gameObject);
				gameManager.resources += 100;
			}
		}
			
			

		RaycastHit hit;
        //Place Buildings at Home Base
        if(other.tag == "Base") {


            //Place
			if ((Input.GetKeyDown("joystick 1 button 1")  || Input.GetKey("o")) && CanBuildSolar()
                    && Physics.Raycast(transform.position, Vector3.down, out hit, 5.0f)) {
				BuildSolar (hit);
            }

			if ((Input.GetKeyDown("joystick 1 button 1")  || Input.GetKey("i")) && CanBuildMine()
				&& Physics.Raycast(transform.position, Vector3.down, out hit, 5.0f)) {
				BuildMine (hit);
			}

			if ((Input.GetKeyDown("joystick 1 button 1")  || Input.GetKey("u")) && CanBuildHouse()
				&& Physics.Raycast(transform.position, Vector3.down, out hit, 5.0f)) {
				BuildHouse (hit);
			}
        }
    }

}
