﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	GameManager gameManager;

	AudioManager audioManager;

	//Health & Damage variables
	public float health = 100.0f;
	public GameObject deathEffect;
	
	//Movement variables
	public Rigidbody rb;
	public float moveForce = 1000.0f;
	public float rotationSpeed = 100.0f;
    public GameObject rayCastPos;
    public GameObject rayCastDir;

	//Weapon variables
	public float fireSpeed = 1.0f;
	private float fireTimer;
	public GameObject projectile;
	public GameObject muzzle;
	public GameObject turret;
    public GameObject muzzleFlash;
	private GameObject currentProjectile;


    //NanoDrone Object variables
    public GameObject nanoDrone;
    public GameObject nanoDroneSpawnLocation;
    private GameObject currentNanoDrone;
	public int droneCost = 100;
    

	//Player Audio
	float enginePitch = 0;
	public AudioSource tankEngine;
	public AudioClip tankShot;
	public AudioClip deathSound;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager> ();
		audioManager = GameObject.FindGameObjectWithTag ("AudioManager").GetComponent<AudioManager> ();

        //Spawn Initial NanoDrone
        currentNanoDrone = Instantiate(nanoDrone, nanoDroneSpawnLocation.transform.position, nanoDroneSpawnLocation.transform.rotation) as GameObject;
	}


	private bool CanSpawnDrone(){
		if (gameManager.resources >= this.droneCost) {
			return true;
		} else {
			return false;
		}
	}
	


    void FixedUpdate() {
        Movement();
    }
		

	private bool HaveFuel(){
		if (gameManager.solarFuel > 0) {
			return true;
		} else {
			return false;
		}
	}



	//This should be hidden in a different script
	//Calculate the fuel useage when the payer is moved based of the set speed of the car
	private void CalculateFuel(bool drive){
		if (drive) {
			int toRemove = (int)(moveForce / 12000.0);
			if (toRemove <= 0) {
				toRemove = 1;
			}

			gameManager.solarFuel -= toRemove;
		} else {
			gameManager.solarFuel -= (int)(rotationSpeed/100.0);
		}
	}


	private void CalculateShot(int damage){
		if (gameManager.resources >= damage) {
			//Instantiate projectile & effect
			currentProjectile = Instantiate (projectile, muzzle.transform.position, muzzle.transform.rotation) as GameObject;
			Instantiate (muzzleFlash, muzzle.transform.position, muzzle.transform.rotation);

			//Play Audio
			audioManager.PlayAudio (tankShot);

			//Reset fire time
			fireTimer = Time.time + fireSpeed;
			currentProjectile.SendMessage ("ChangeDamage", damage);

			gameManager.resources -= damage;
		}
	}


	// Make the rover take damage
	public void takeDamage(float dmg){
		health -= dmg;
		
		if (health <= 0) {
			Instantiate (deathEffect, transform.position, transform.rotation);
			gameManager.playerAlive = false;
			audioManager.PlayAudio(deathSound);
			Destroy (this.gameObject);
		}
	}


	// Update is called once per frame
	void Update () {

		RespawnDrone ();
		Shoot ();

		enginePitch = Mathf.Clamp(rb.velocity.magnitude/10, 0.5f, 2.5f);
		tankEngine.pitch = enginePitch;
	}




	void RespawnDrone(){
		//Respawn Nano Drone
		if ((Input.GetKey("joystick 1 button 3") || Input.GetKey("h")) && CanSpawnDrone()) {

			if (currentNanoDrone != null) {
				Destroy (currentNanoDrone);
			}
			gameManager.resources -= this.droneCost;

			currentNanoDrone = Instantiate(nanoDrone, nanoDroneSpawnLocation.transform.position, nanoDroneSpawnLocation.transform.rotation) as GameObject;
		}
	}


	//Students To Complete
	void Movement(){

		//If wheels are touching the ground
		RaycastHit hit;
		if(Physics.Raycast(rayCastPos.transform.position, rayCastDir.transform.position - rayCastPos.transform.position, out hit, 3.0f)) {

			if (hit.transform.tag == "Environment" && HaveFuel()) {

				//Forward and Backward movement
				if (Input.GetKey ("w")) {
					rb.AddRelativeForce (Vector3.forward * moveForce * Time.fixedDeltaTime);
					CalculateFuel (true);
				} else if (Input.GetKey ("s")) {
					rb.AddRelativeForce (Vector3.back * moveForce * Time.fixedDeltaTime);
					CalculateFuel (true);
				}

				//Left and Right rotation
				if (Input.GetKey ("a")) {
					transform.Rotate (Vector3.up * -rotationSpeed * Time.fixedDeltaTime);
					CalculateFuel (false);
				} else if (Input.GetKey ("d")) {
					transform.Rotate (Vector3.up * rotationSpeed * Time.fixedDeltaTime);
					CalculateFuel (false);
				}
			}
		}
	}



	//Students do this
	void Shoot(){
		if (Input.GetMouseButtonDown (0) && Time.time > fireTimer) {
			CalculateShot (20);
		}

		if (Input.GetKey ("j") && Time.time > fireTimer) {
			CalculateShot(10);
		}	 

		if (Input.GetKey ("k") && Time.time > fireTimer) {
			CalculateShot(20);
		}	

		if (Input.GetKey ("l") && Time.time > fireTimer) {
			CalculateShot(50);
		}	
	}
}
