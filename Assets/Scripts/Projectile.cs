﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	AudioManager audioManager;

	public float projectileSpeed = 50.0f;

	private float damage;

	private float test;

	public float lifeTime = 5.0f;

	public GameObject detonateEffect;

	public AudioClip shotExplode;

	// Use this for initialization
	void Start () {
		Destroy (this.gameObject, lifeTime);

		audioManager = GameObject.FindGameObjectWithTag ("AudioManager").GetComponent<AudioManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		Movement ();
	}

	public virtual void Movement(){
		transform.position += transform.forward * projectileSpeed * Time.deltaTime;
	}



	void OnTriggerEnter(Collider otherObject){
		if (otherObject.tag == "Enemy") {
			otherObject.GetComponent<Enemy>().takeDamage(damage);
			Instantiate (detonateEffect, transform.position, transform.rotation);
			audioManager.PlayAudio(shotExplode);
			Destroy (this.gameObject);
		}
	}

	public void ChangeDamage(int d){
		this.damage = (float)d;
	}
}
