﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	GameManager gameManager;

	Player player;

	//Links to GUI objects
	public Text resources;
	public Text fuel;
	public Text timeRemaining;
	public Slider health;
    public Text score;

    //GameOver Screen
    public GameObject gameOver;
    public Text status;
    public Text timeSurvived;
    public Text finalScore;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager> ();
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<Player> ();

	}
	
	// Update is called once per frame
	void Update () {

		//Update GUI Elements
		resources.text = "Resources: " + gameManager.resources;
		fuel.text = " Fuel: " + gameManager.solarFuel;

		timeRemaining.text = "Time Remaining: " + gameManager.timeRemaining.ToString("n2");
		health.value = player.health;
        score.text = "Score: " + gameManager.score;



        //Game Over
        if (gameManager.gameOver) {
            gameOver.SetActive(true);

            if (gameManager.playerAlive)
                status.text = "Status: Mission Accomplished!";
            else
                status.text = "Status: You Were Destroyed!";

            timeSurvived.text = "Time Survived: " + gameManager.timeSurvived.ToString("n2");

            finalScore.text = "Score: " + gameManager.score;
        }
	}
}
